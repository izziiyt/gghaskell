module MergeSort
(mergeSort) where
import qualified Data.List.Split as Sp

merge :: (Ord t) => [[t]] -> [t]
merge [xs] = xs
merge [xs,[]] = xs
merge [[],ys] = ys
merge [xs,ys]
    | x < y = x : merge [tail xs,ys]
    | otherwise = y : merge [xs,tail ys]
    where x = head xs
          y = head ys
merge _ = error "Fxck You !!"

mergeSort' :: (Ord t) => [[t]] -> [[t]]
mergeSort' [] = []
mergeSort' [x] = [x]
mergeSort' xs = mergeSort' . map merge $ Sp.chunksOf 2 xs

mergeSort :: (Ord t) => [t] -> [t]
mergeSort [] = []
mergeSort [x] = [x]
mergeSort xs = head . mergeSort' $ Sp.chunksOf 1 xs
