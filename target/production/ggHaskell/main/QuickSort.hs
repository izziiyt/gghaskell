module QuickSort
(
quickSort,
quickSort',
kthSelect
)where

quickSort :: (Ord t) => (t -> t -> Bool) -> [t] -> [t]
quickSort _ [] = []
quickSort f (x:xs) = left ++ [x] ++ right
    where left = quickSort f [a | a <- xs,f a x]
          right = quickSort f [a | a <- xs,not $ f a x]

quickSort' :: (Ord t) => (t -> t -> Bool) -> [t] -> [t]
quickSort' _ [] = []
quickSort' f (x:xs) = quickSort' f left ++ [x] ++ quickSort' f right
    where (left,right) = split xs [] []
          split [] ys zs = (ys,zs)
          split (a:as) ys zs
              | f a x = split as (a:ys) zs
              | otherwise = split as ys (a:zs)

-- 0 origin k-th number
kthSelect :: (Ord t) => (t -> t -> Bool) -> Int -> [t] -> t
kthSelect f k xs
    | k >= length xs = error "K is bigger than size of the list."
    | otherwise = kthSelect' f k xs

kthSelect' :: (Ord t) => (t -> t -> Bool) -> Int -> [t] -> t
kthSelect' _ _ [x] = x
kthSelect' f k (x:xs) = kthSelect' f newk rs
    where (newk,rs) = choice k xs [] []
          choice l [] ys zs
              | l == 0  = (0,[x])
              | l > 0 = (l-1,zs)
              | otherwise = (k,ys)
          choice l (w:ws) ys zs
              | f w x = choice (l - 1) ws (w:ys) zs
              | otherwise = choice l ws ys (w:zs)