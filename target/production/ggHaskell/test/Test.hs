module Main where

import MergeSort
import QuickSort
import Test.Hspec

main:: IO ()
main = hspec $ do
    describe "mergeSort" $ do
        it "case 1" $ do
            mergeSort [2,4,6,9,3,1,7] `shouldBe` [1,2,3,4,6,7,9]
        it "case 2" $ do
            mergeSort [5,4,3,2,1] `shouldBe` [1,2,3,4,5]
        --it "case 3" $ do
        --    mergeSort [] `shouldBe` []
        it "case 4" $ do
            mergeSort [5] `shouldBe` [5]
    describe "quickSort" $ do
        it "case 1" $ do
            quickSort (<=) [2,4,6,9,3,1,7] `shouldBe` [1,2,3,4,6,7,9]
        it "case 2" $ do
            quickSort (<=) [5,4,3,2,1] `shouldBe` [1,2,3,4,5]
        --it "case 3" $ do
        --    quickSort (<=) [] `shouldBe` []
        it "case 4" $ do
            quickSort (<=) [5] `shouldBe` [5]
    describe "quickSort'" $ do
        it "case 1" $ do
            quickSort' (<=) [2,4,6,9,3,1,7] `shouldBe` [1,2,3,4,6,7,9]
        it "case 2" $ do
            quickSort' (<=) [5,4,3,2,1] `shouldBe` [1,2,3,4,5]
        --it "case 3" $ do
        --    quickSort (<=) [] `shouldBe` []
        it "case 4" $ do
            quickSort' (<=) [5] `shouldBe` [5]
    describe "kthSelect" $ do
        it "case 1" $ do
            kthSelect (<=) 3 [2,4,6,9,3,1,7] `shouldBe` 4
        it "case 2" $ do
            kthSelect (<=) 0 [5,4,3,2,1] `shouldBe` 1
        it "case 3" $ do
            kthSelect (<=) 5 [10,2,8,7,3,0] `shouldBe` 10
        it "case 4" $ do
            kthSelect (<=) 3 [3,2,4,5,1] `shouldBe` 4
